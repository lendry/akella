package ru.alvikl;

import java.util.Date;

public class Person {
    private String firstName;
    private String lastName;
    private String middleName;
    //    private String login;
//    private String email;
    private Date birthday;

    public Person() {
    }

    public Person(String firstName, String lastName, String middleName, Date birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthday = birthday;
    }
}