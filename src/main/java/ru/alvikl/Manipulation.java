package ru.alvikl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Manipulation {
    // merge 2 genes at a random point
    private static int[] geneMerge(int[] gene1, int[] gene2) {
        if (gene1.length != gene2.length)
            System.out.println("merge incompatible genes");
        Random rand = new Random();
        int point = rand.nextInt(gene1.length - 2) + 1;

        int[] gene = new int[gene1.length];
        for (int i = 0; i < gene1.length; i++)
            gene[i] = i < point ? gene1[i] : gene2[i];
        return gene;
    }

    // merge 2 specimen at a random gene.
    public static Specimen merge(Specimen specimen1, Specimen specimen2) {
        // добавить логику проверки point. 1ый ген или последний.
        Random rand = new Random();
        List<int[]> genes = new ArrayList<>();
        Specimen specimen;
        int size = specimen1.size();
        int point = rand.nextInt(size);
//        System.out.println("specimen merge point: " +point);
//        for(int i=0;i<point;i++){
//            int[] gene=specimen1.getGene(i);
//            genes.add(Arrays.copyOf(gene,gene.length));
//        }
//        try {
//            genes.add(geneMerge(specimen1.getGene(point), specimen2.getGene(point)));
//        }catch(Exception e){
//
//        }
//        for(int i=point+1;i<size;i++){
//            int[] gene=specimen2.getGene(i);
//            genes.add(Arrays.copyOf(gene,gene.length));
//        }
        for (int i = 0; i < size; i++) {
            int[] gene;
            if (i == point)
                gene = geneMerge(specimen1.getGene(point), specimen2.getGene(point));
            else {
                specimen = i < point ? specimen1 : specimen2;
                gene = Arrays.copyOf(gene=specimen.getGene(i), gene.length);
            }
            genes.add(gene);
        }

        return new Specimen(genes); //Speciment
    }

    // increase/decrease (50/50) break position with the specified probability
    private static void geneMutation(int[] gene, double probability) {
        Random rand = new Random();
        for (Integer i = 1; i < gene.length - 1; i++) {
            float rn = rand.nextFloat();
            if (rn < probability) {
                gene[i] += rand.nextInt(2) < 1 ? -1 : 1;
            }
        }
    }

    public static void mutation(Specimen specimen, double probable) {
        for (int i = 0; i < specimen.size(); i++) {
            int[] gene = specimen.getGene(i);
            geneMutation(gene, probable);
        }
    }
}
