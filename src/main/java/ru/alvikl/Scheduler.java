package ru.alvikl;

import java.util.List;

public class Scheduler {

//    // Min time interval between breaks
//    private int minTimeBetweenBreaks = 2;
//    // Max time interval between breaks
//    private int maxTimeBetweenBreaks = 5;

    private int populationSize = 50;
    List<Operator> operators;
    Constraint constraint;
    List<Specimen> population;
    private int iterationNumber = 15;

    public Scheduler(List<Operator> operators, Constraint constraint, int iterarationNumber, int populationSize) {
        this.operators = operators;
        this.constraint = constraint;
        this.iterationNumber=iterarationNumber;
        this.populationSize=populationSize;
    }

    public void createSchedule() {

        SpecimenInitializer specimenInitializer = new SpecimenInitializer(operators,constraint);
        CalculateDeviation calculateDeviation = new CalculateDeviation(operators,constraint);
        Population population = new Population(populationSize, specimenInitializer,calculateDeviation);
        population.firstGeneration();

        Specimen minDeviationSpecimentOverAllPopulations=population.getMinDeviationSpecimen();
        //for(int i=0;i<iterationNumber; i++){
        int i = 1;
        while (population.getMinDeviationSpecimen().getDeviation()>1 && i < iterationNumber) {
            population.nextGeneration();
            if(minDeviationSpecimentOverAllPopulations.getDeviation()>population.getMinDeviationSpecimen().getDeviation())
                minDeviationSpecimentOverAllPopulations=population.getMinDeviationSpecimen();
            i++;
        }
        System.out.println(minDeviationSpecimentOverAllPopulations.toString());

    }
}
