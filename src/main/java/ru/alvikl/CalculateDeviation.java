package ru.alvikl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateDeviation {
    List<Operator> operators;
    Constraint constraint;
    Map<Integer,Integer> distributionNumberOperatorsInTime;

    public CalculateDeviation(List<Operator> operators, Constraint contraint) {
        this.operators = operators;
        this.constraint=contraint;
        this.distributionNumberOperatorsInTime=distributionNumberOperatorsInTime();
    }

    public Map<Integer, Integer> getDistributionNumberOperatorsInTime() {
        return distributionNumberOperatorsInTime;
    }

    private double totalExcessWorkingTimeBetweenBreaks(int[] gene, Operator operator) {
        int totaltime = 0;
        int workDuration;
        //List<Integer> breaks=chromosome.breaks.get(i);
        int[] breaksDuration = operator.getBreaksDuration();
        for (int j = 1; j < gene.length; j++) {
            workDuration = gene[j] - (gene[j - 1] + breaksDuration[j - 1]);
            totaltime += workDuration > constraint.getMaxTimeBetweenBreaks() ?
                    workDuration - constraint.getMaxTimeBetweenBreaks() : 0;
        }
        return totaltime;
    }

    private double totalLackWorkingTimeBetweenBreaks(int[] gene,Operator operator) {
        int totaltime = 0;
        int workDuration;
        //List<Integer> breaks=chromosome.breaks.get(i);
        int[]  breaksDuration = operator.getBreaksDuration();
        for (int j = 1; j < gene.length; j++) {
            workDuration = gene[j] - (gene[j - 1] + breaksDuration[j - 1]);
            totaltime += workDuration < constraint.getMinTimeBetweenBreaks() ?
                    constraint.getMinTimeBetweenBreaks() - workDuration : 0;
        }
        return totaltime;
    }

    private Map<Integer,Integer> distributionBreaksInTime(Specimen specimen){
        Map<Integer,Integer> res= new HashMap<>();
        for(int i=0;i<specimen.size();i++){
            int[] gene=specimen.getGene(i);
            Operator operator=operators.get(i);
            int[]  breaksDuration = operator.getBreaksDuration();
            for(int j=0;j<gene.length;j++){
                for(int k=0;k<breaksDuration[j];k++)
                    res.put(gene[j+k],res.getOrDefault(gene[j+k],0)+1);
            }
        }
        return res;
    }

    private  Map<Integer,Integer> distributionNumberOperatorsInTime(){
        Map<Integer,Integer> res= new HashMap<>();
        for(Operator operator: operators)
            for(int i=operator.getShiftStartTime();i<operator.getShiftEndTime();i++)
                res.put(i,res.getOrDefault(i,0)+1);
        return res;
    }

    //calculates the amount of excess of the allowed number of breaks (for all time segments)
    private double sumExcessAllowedNumberOfBreaks(Specimen specimen){
        double deviation=0;
        Map<Integer,Integer> distributionNumberOperatorsInTime = getDistributionNumberOperatorsInTime();
        Map<Integer,Integer> distributionBreaksInTime = distributionBreaksInTime(specimen);
        //go through all the time intervals where there are breaks
        for(Map.Entry<Integer,Integer> entry: distributionBreaksInTime.entrySet()){
            double limitValue=
            distributionNumberOperatorsInTime.getOrDefault(entry.getKey(), 0)
                    *constraint.getOnBreakPercent();
            if (limitValue<entry.getValue())
                deviation+=entry.getValue()-limitValue;
        }
        return deviation;
    }

    protected double totalGeneDeviation(int[] gene, Operator operator){
        return  totalLackWorkingTimeBetweenBreaks(gene, operator)
                +totalExcessWorkingTimeBetweenBreaks(gene, operator);
    }
    private double sumGeneDeviationsOfSpecimen(Specimen specimen){
        double deviation=0;
        if (specimen==null)
            return deviation;
        for(int i=0;i<specimen.size();i++) {
            int[] gene = specimen.getGene(i);
            Operator operator = operators.get(i);
            deviation += totalGeneDeviation(gene, operator);
        }
        return deviation;
    }

    protected double totalSpecimentDeviation(Specimen specimen){
        double allDeviation=0;
        if (specimen==null)
            return allDeviation;
        allDeviation+=sumGeneDeviationsOfSpecimen(specimen);
        allDeviation+=sumExcessAllowedNumberOfBreaks(specimen);
        return allDeviation;
    }

    public void updateDeviation(Specimen specimen){
        specimen.setDeviation(totalSpecimentDeviation(specimen));
    }

}
