package ru.alvikl;

public class Constraint {
    // Min time interval between breaks
    private int minTimeBetweenBreaks=5;
    // Max time interval between breaks
    private int maxTimeBetweenBreaks=15;

    // procent
    private double onBreakPercent =0.30;


    public Constraint(){

    }

    public Constraint(int minTimeBetweenBreaks, int maxTimeBetweenBreaks) {
        this.minTimeBetweenBreaks = minTimeBetweenBreaks;
        this.maxTimeBetweenBreaks = maxTimeBetweenBreaks;
    }

    public void setMinTimeBetweenBreaks(int minTimeBetweenBreaks) {
        this.minTimeBetweenBreaks = minTimeBetweenBreaks;
    }

    public void setMaxTimeBetweenBreaks(int maxTimeBetweenBreaks) {
        this.maxTimeBetweenBreaks = maxTimeBetweenBreaks;
    }

    public int getMinTimeBetweenBreaks(){
        return minTimeBetweenBreaks;
    }

    public int getMaxTimeBetweenBreaks(){
        return maxTimeBetweenBreaks;
    }

    public double getOnBreakPercent() {
        return onBreakPercent;
    }

    public void setOnBreakPercent(double onBreakPercent) {
        this.onBreakPercent = onBreakPercent;
    }
}
