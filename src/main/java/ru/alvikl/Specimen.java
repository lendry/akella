package ru.alvikl;

import java.util.Arrays;
import java.util.List;

public class Specimen {
    private List<int []> genes;

    // the amount of deviation from the constraints of problem
    double deviation;

    //probability of selection relative to other specimens in the population
    double selectionProbability;

    public Specimen(List<int[]> genes) {
        this.genes=genes;
    }

    public List<int[]> getGenes() {
        return genes;
    }

    public int size(){
        return genes.size();
    }

    public void setGenes(List<int[]> genes) {
        this.genes = genes;
    }

    public int[] getGene(int indx){
        return genes.get(indx);
    }

    public double getDeviation() {
        return deviation;
    }

    public void setDeviation(double deviation) {
        this.deviation = deviation;
    }

    public double getSelectionProbability() {
        return selectionProbability;
    }

    public void setSelectionProbability(double selectionProbability) {
        this.selectionProbability = selectionProbability;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int[] gene : genes)
            str.append(Arrays.toString(gene) + " ");
        str.append(this.getDeviation() + " "+ this.getSelectionProbability());
        return str.toString();
    }

}
