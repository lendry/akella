package ru.alvikl;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Operator> operators = new ArrayList<>();
        Operator operator = new Operator("Vasya","","",
                new GregorianCalendar(1982,10,1).getTime(),
                2,34,3, new int[]{0,1,2,1,0});
        operators.add(operator);

        operator = new Operator("Timon","","",
                new GregorianCalendar(1982,01,1).getTime(),
                2,34,1, new int[]{0,1,2,1,0});
        operators.add(operator);

        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                2,34,1, new int[]{0,1,2,1,0});
        operators.add(operator);
        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                2,34,1, new int[]{0,1,2,1,0});operators.add(operator);
        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                22,47,1, new int[]{0,1,2,0});operators.add(operator);
        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                17,47,1, new int[]{0,1,2,1,0});operators.add(operator);
        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                18,34,1, new int[]{0,1,2,0});operators.add(operator);
        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                10,24,1, new int[]{0,2,0});
        operators.add(operator);

        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                34,47,1, new int[]{0,2,0});
        operators.add(operator);

        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                10,22,1, new int[]{0,1,0});
        operators.add(operator);

        operator = new Operator("Pumba","","",
                new GregorianCalendar(1981,04,1).getTime(),
                2,14,1, new int[]{0,1,0});
        operators.add(operator);


        Constraint constraint=new Constraint(5,15);

        Scheduler scheduler = new Scheduler(operators,constraint,15000,200);


        scheduler.createSchedule();


        //
    }
}
