package ru.alvikl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Population {
    SpecimenInitializer specimeninitializer;
    CalculateDeviation calculateDeviation;
    double mutationProbability=0.05;
    int size;

    List<Specimen> specimens;

    int minDeviationSpecimenIndex = -1;

    public Population(int size, SpecimenInitializer specimeninitializer, CalculateDeviation calculateDeviation) {
        this.specimeninitializer = specimeninitializer;
        this.calculateDeviation = calculateDeviation;
        this.size = size;
    }

    //get the index of the specimen corresponding to the specified real number
    // value in the range from 0 to 1
    private int getIndex(float value) {
        double last = 0;
        for (int i = 0; i < size; i++) {
            if (value >= last && value < last + specimens.get(i).getSelectionProbability()) {
                return i;
            } else
                last += specimens.get(i).getSelectionProbability();
        }
        return size - 1;
    }

    public void firstGeneration() {

        specimens = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Specimen specimen = new Specimen(specimeninitializer.initialize());
            calculateDeviation.updateDeviation(specimen);
            specimens.add(specimen);
        }
        updateSelectionProbability();
        updateMinDeviationSpecimenIndex();
    }

    public void nextGeneration() {
        Random rand = new Random();
        Specimen specimen1;
        Specimen specimen2;
        List<Specimen> newSpecimens = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            int indx1 = 0;
            int indx2 = 0;
            int counter = 0;
            while (indx1 == indx2 && counter++ < 10) {
                indx1 = getIndex(rand.nextFloat());
                indx2 = getIndex(rand.nextFloat());
            }
            specimen1 = specimens.get(indx1);
            specimen2 = specimens.get(indx2);
            Specimen specimen= Manipulation.merge(specimen1, specimen2);
            Manipulation.mutation(specimen,mutationProbability);
            calculateDeviation.updateDeviation(specimen);
            newSpecimens.add(specimen);
        }
        specimens=newSpecimens;
        updateSelectionProbability();
        updateMinDeviationSpecimenIndex();
    }

    // calculation  probability of selection for each specimen
    //  p[i]= 1/d[i] / sum(1/d[j])
    public void updateSelectionProbability() {
        double last = 0;
        double sumOfInverseDeviations = sumOfInverseDeviations();
        for (Specimen specimen : specimens) {
            specimen.setSelectionProbability(1 / specimen.getDeviation() / sumOfInverseDeviations);
        }
    }

    // calculation sum of inverse deviations
    private double sumOfInverseDeviations() {
        double res = 0;//1 / specimens.get(0).getDeviation();
        for (Specimen specimen : specimens) {
            res += 1 / specimen.getDeviation();
        }
        return res;
    }

    // find index of specimen with the  minimal deviation
    private void updateMinDeviationSpecimenIndex(){
        minDeviationSpecimenIndex=0;
        double minDeviation = specimens.get(0).getDeviation();
        for (int i = 1; i < size; i++) {
            if (minDeviation > specimens.get(i).getDeviation()) {
                minDeviation = specimens.get(i).getDeviation();
                minDeviationSpecimenIndex = i;
            }
        }
    }

    public Specimen getMinDeviationSpecimen() {
        return specimens.get(minDeviationSpecimenIndex);
    }
}
