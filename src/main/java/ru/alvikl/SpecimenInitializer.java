package ru.alvikl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SpecimenInitializer {
    private List<Operator> operators;
    private Constraint constraint;

    public SpecimenInitializer(List<Operator> operators, Constraint constraint) {
        this.operators = operators;
        this.constraint = constraint;
    }

    private int [] geneInitializer(Operator operator) {
        Random rand = new Random();
        // randomly fill in the intervals of working time, call them "bags".
        // each bags is either the number of time slots from the start of the shift to the first break,
        // or between breaks, or from the last break to the end of the shift.

        // total bags one more than the number of breaks
        int bagCount = (operator.getBreaksDuration().length-2) + 1;

        List<Integer> bags = new ArrayList<>();

        //access to bags only through bagLinks.
        //bags.get(bagLinks.get(bagIndex))
        //if the bag is full, delete the according bagLink element
        List<Integer> bagLinks = new LinkedList<>();

        for (int i = 0; i < bagCount; i++) {
            //start at minimal time
            bags.add(constraint.getMinTimeBetweenBreaks());
            bagLinks.add(i);
        }

        //total number of time slots to will put in bags
        // shift length -  the total rest time slots - number of filled time slots
        int slotsCount = operator.getShiftEndTime() - operator.getShiftStartTime()
                - operator.getTotalBreaksDuration() -
                bagCount * constraint.getMinTimeBetweenBreaks();

        for (int i = 0; i < slotsCount; i++) {
            int bagIndex = rand.nextInt(bagCount);
            int intervalLength = bags.get(bagLinks.get(bagIndex)) + 1;

            bags.set(bagLinks.get(bagIndex), intervalLength);
            //if the bag is full, delete the according bagLink element, to avoid adding more to the bag.
            if (intervalLength >= constraint.getMaxTimeBetweenBreaks()) {
                bagLinks.remove(bagIndex);
                bagCount--;
            }
        }

        // generation array of breaks position
        int[] positions = new int[operator.getBreaksDuration().length];
        int pos = operator.getShiftStartTime();
        positions[0]=pos;
        for (int i = 0; i < bags.size(); i++) {
            pos += bags.get(i) + operator.getBreaksDuration()[i];
            positions[i+1]=pos;
        }

        return positions;
    }

    public List<int[]> initialize(){
        List<int[]> genes=new ArrayList<>();
        for (Operator operator: operators){
            genes.add(geneInitializer(operator));
        }
        return genes;
    }
}
