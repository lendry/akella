package ru.alvikl;


import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Operator extends Person {
    private int shiftStartTime;
    private int shiftEndTime;
    private int groupId;
    int[] breaksDuration;

    public Operator(String firstName, String lastName, String middleName, Date birthday, int shiftStartTime, int shiftEndTime, int groupId, int[] breaksDuration) {
        super(firstName, lastName, middleName, birthday);
        this.shiftStartTime = shiftStartTime;
        this.shiftEndTime = shiftEndTime;
        this.groupId = groupId;
        this.breaksDuration = breaksDuration;
    }

    public Operator(String firstName, String lastName, String middleName, Date birthday, int groupId, Shift shift ) {
        super(firstName, lastName, middleName, birthday);
        this.groupId = groupId;
        this.shiftStartTime = shift.StartTime;
        this.shiftEndTime = shift.EndTime;
        this.breaksDuration = shift.breaksDuration;
    }

//    public Operator(int shiftStartTime, int shiftEndTime, List<Integer> breaksDuration, int groupId) {
//        this.shiftStartTime = shiftStartTime;
//        this.shiftEndTime = shiftEndTime;
//        this.groupId = groupId;
//        this.breaksDuration = breaksDuration;
//    }

//    public int getBreaksCount(){
//        return breaksDuration.size()-2;
//    }

    public int getTotalBreaksDuration(){
        int res=0;
        for(Integer duration:breaksDuration){
            res+=duration;
        }
        return res;
    }

    public int[]getBreaksDuration() {
        return breaksDuration;
    }

    public int getShiftStartTime() {
        return shiftStartTime;
    }

    public int getShiftEndTime() {
        return shiftEndTime;
    }

    public int getGroupId() {
        return groupId;
    }

    @Override
    public String toString() {
        return "Operator{" +
                "shiftStartTime=" + shiftStartTime +
                ", shiftEndTime=" + shiftEndTime +
                '}';
    }
}

